import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 4.11.2017.
 */

public class IntegrityHelper {

    private static final String crontabFileName = "./_cron";
    private static final String scriptFilePath = "./_scriptCron.sh";

    private static final String CREATED = "created";
    private static final String DELETED = "deleted";
    private static final String ALTERED = "altered";

    private String monitoringFolderPath;
    private String registryFilePath;
    private String logFilePath;
    private String hashAlgorithm;
    private String privateKeyFilePath;
    private String publicKeyFilePath;
    private int intervalTime; // in minutes

    private HashHelper hashHelper;
    private CryptUtil cryptUtil;


    IntegrityHelper(String monitoringFolderPath, String registryFilePath, String logFilePath, String hashAlgorithm, String privateKeyFilePath, String publicKeyFilePath, int intervalTime) {
        this.monitoringFolderPath = monitoringFolderPath;
        this.registryFilePath = registryFilePath;
        this.logFilePath = logFilePath;
        this.hashAlgorithm = hashAlgorithm;
        this.privateKeyFilePath = privateKeyFilePath;
        this.publicKeyFilePath = publicKeyFilePath;
        this.intervalTime = intervalTime;

        this.hashHelper = new HashHelper(this.hashAlgorithm);
        this.cryptUtil = new CryptUtil();
    }

    IntegrityHelper(){}

    /**
     * index/create necessary files and
     * starts integrity schedule tasks
     */
    void start() {
        createFiles();
        ArrayList<File> monitoringFileList = getAllFiles(monitoringFolderPath);
        indexRegistryFile(monitoringFileList);
        startUnixCalls();
    }

    /**
     * stops integrity schedule tasks
     */
    void stop() {
        stopUnixCalls();
    }

    /**
     * verifies registry file and writes changes to log file
     */
    void unixCall() {
        FileWriter logWriter;
        try {
            logWriter = new FileWriter(logFilePath, true);
            boolean verify = cryptUtil.verifyFile(registryFilePath, publicKeyFilePath, hashAlgorithm);
            if (!verify) {
                logWriter.write(Utils.getDate() + ": verification failed\n");
                logWriter.close();
            } else {
                compareFolderChanges(logWriter);
                logWriter.close();
            }
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in writing log file: " + e.getMessage(), 1);
        }
    }

    /**
     * compares and writes changes to log file
     */
    private void compareFolderChanges(FileWriter logWriter) throws IOException {
        ArrayList<File> fileList = getAllFiles(monitoringFolderPath);
        HashMap<String, String> fileHashPair = getRegistryFileContent();
        for (File newFolderItem : fileList) {
            if (fileHashPair.get(newFolderItem.getAbsolutePath()) == null) {
                logWriter.write(Utils.getDate() + " " + newFolderItem.getAbsolutePath() + " " + CREATED + "\n");
            } else {
                String newFileHash = hashHelper.hash(newFolderItem);
                String oldHash = fileHashPair.get(newFolderItem.getAbsolutePath());
                if (!newFileHash.equals(oldHash)) {
                    logWriter.write(Utils.getDate() + " " + newFolderItem.getAbsolutePath() + " " + ALTERED + "\n");
                }
                fileHashPair.remove(newFolderItem.getAbsolutePath());
            }
        }
        if (fileHashPair.size() != 0) {
            for (String deletedFilePath : fileHashPair.keySet()) {
                logWriter.write(Utils.getDate() + " " + deletedFilePath + " " + DELETED + "\n");
            }
        }
    }

    /**
     * Gives us a hashmap that contains indexed files and its hashes
     */
    private HashMap<String, String> getRegistryFileContent() {
        HashMap<String, String> fileHash = new HashMap<>();
        try {
            FileReader fr = new FileReader(registryFilePath);
            BufferedReader br = new BufferedReader(fr);
            String line;

            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("##signature") && !line.isEmpty()) {
                    String[] fileHashPair = line.split(" ");
                    fileHash.put(fileHashPair[0], fileHashPair[1]);
                }
            }
            br.close();
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in reading registry file." + e.getMessage(), 1);
        }
        return fileHash;
    }


    private void startUnixCalls() {
        /**
         *  UNIX CRONTAB ACCEPTS ONLY !
         *
         minute 	0-59    The exact minute that the command sequence executes
         hour 	    0-23    The hour of the day that the command sequence executes
         day 	    1-31    The day of the month that the command sequence executes
         month 	    1-12    The month of the year that the command sequence executes
         weekday 	0-6     The day of the week that the command sequence executes (Sunday = 0, Monday = 1, Tuesday = 2, and so forth)
         *
         */
        try {
            File file = new File(crontabFileName);
            File scriptFile = new File(scriptFilePath);
            File currentPath = new File("./");

            PrintWriter scriptWriter = new PrintWriter(scriptFile);
            scriptWriter.write("#!/bin/sh");
            scriptWriter.write("\n");
            scriptWriter.write("java -cp " + currentPath.getAbsolutePath() + " " + getUnixCallPath());
            scriptWriter.write("\n");
            scriptWriter.close();

            PrintWriter fileWriter = new PrintWriter(file);
            fileWriter.write(parseIntervalTime() + " sh " + scriptFile.getAbsolutePath());
            fileWriter.write("\n");
            fileWriter.close();

            Runtime terminal = Runtime.getRuntime();
            terminal.exec("crontab " + file.getAbsolutePath());
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in starting cron job: " + e.getMessage(), 1);
        }
    }

    private void stopUnixCalls() {
        try {
            File cronFile = new File(crontabFileName);
            clearFileContent(cronFile);
            Runtime terminal = Runtime.getRuntime();
            terminal.exec("crontab " + cronFile.getAbsolutePath());
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in stopping cron job: " + e.getMessage(), 1);
        }
    }

    private String parseIntervalTime() {
        int cronHour = 60;
        int cronDay = 60 * 24;
        if (intervalTime > cronDay || intervalTime < 0) {
            Utils.printErrorAndUsage("Interval time must be less than 1 day (1440 minutes)", 1);
        } else if (intervalTime == cronDay) {
            return "* * */1 * *";
        } else if (intervalTime > cronHour) {
            int lHour = intervalTime / 60;
            int lMinute = intervalTime % 60;
            return "*/" + lMinute + " */" + lHour + " * * *";
        } else if (intervalTime == cronHour) {
            return "* */1 * * *";
        } else {
            if (intervalTime != 0) {
                return "*/" + intervalTime + " * * * *";
            }
        }
        return "*/1 * * * *";
    }

    private String getUnixCallPath() {
        File monitoringFolder = new File(monitoringFolderPath);
        File registryFile = new File(registryFilePath);
        File logFile = new File(logFilePath);
        File publicKeyFile = new File(publicKeyFilePath);
        return "integrity"
                + " -p " + monitoringFolder.getAbsolutePath()
                + " -r " + registryFile.getAbsolutePath()
                + " -l " + logFile.getAbsolutePath()
                + " -h " + hashAlgorithm
                + " -k " + publicKeyFile.getAbsolutePath();
    }

    private void indexRegistryFile(ArrayList<File> fileList) {
        try {
            FileWriter fileWriter = new FileWriter(registryFilePath);
            for (File file : fileList) {
                fileWriter.write(file.getAbsolutePath() + " " + hashHelper.hash(file) + "\n");
            }
            fileWriter.close();
            fileWriter = new FileWriter(registryFilePath, true);
            fileWriter.write("##signature: " + cryptUtil.signFile(registryFilePath, privateKeyFilePath, hashAlgorithm));
            fileWriter.close();
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in writing hashes in registry file: " + e.getMessage(), 1);
        }
    }

    /**
     * creates log and registry files
     */
    private void createFiles() {
        File registryFile = new File(registryFilePath);
        File logFile = new File(logFilePath);
        File cronFile = new File(crontabFileName);
        File scriptFile = new File(scriptFilePath);

        try {
            registryFile.createNewFile();
            logFile.createNewFile();
            cronFile.createNewFile();
            scriptFile.createNewFile();

            setFileModes(registryFile);
            setFileModes(logFile);
            setFileModes(cronFile);
            setFileModes(scriptFile);

        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in creating registry file and log file, probably path is invalid.", 1);
        }
    }

    /**
     * finds all files recursively in the given path
     */
    private ArrayList<File> getAllFiles(String filePath) {
        ArrayList<File> fileList = new ArrayList<>();
        File file = new File(filePath);
        if (!file.exists()) {
            Utils.printErrorAndUsage("Invalid folder path for monitoring files.", 1);
        } else {
            findFiles(fileList, file);
        }
        return fileList;
    }

    private void findFiles(ArrayList<File> fileList, File dir) {
        File[] files = dir.listFiles();
        for (File temp : files) {
            if (temp.isDirectory()) {
                findFiles(fileList, temp);
            } else if (temp.isFile()) {
                fileList.add(temp);
            }
        }
    }

    private void clearFileContent(File file) {
        try {
            PrintWriter printWriter = new PrintWriter(file);
            printWriter.print("");
            printWriter.close();
        } catch (FileNotFoundException e) {
            Utils.printErrorAndUsage("Error in clearing " + file.getName() + " " + e.getMessage(), 1);
        }
    }

    private void setFileModes(File file) {
        file.setReadable(true, false);
        file.setWritable(true, false);
        file.setExecutable(true, false);
    }
}
