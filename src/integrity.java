/**
 * Author:      Ebubekir Yigit and Huseyin Kayayurt
 * <p>
 * <p>
 * File:        integrity.java
 * <p>
 * <p>
 * Purpose:     Hash Function and Digital Signature with RSA
 * <p>
 * In the project, it is aimed to write a running program in minutes
 * to track the changes in the path of the given file.
 * First, all the files in the given folder are found and saved with hash values.
 * The recorded file is signed with RSA. In the given minute,
 * an executable program checks the signature for the credibility of the file.
 * If the signature is verified, changes in the folder are saved in the log file.
 * <p>
 * <p>
 * Compile:     javac integrity.java
 * <p>
 * <p>
 * Run:         java integrity stop
 * &
 * java integrity start -p <Monitoring Folder Path>
 * -r <Registry File Path>
 * -l <Log File Path>
 * -h <Hash Algorithm>
 * -k <Private Key File> <Public Key File>
 * -i <Interval Time>
 * <p>
 * <p>
 * <p>
 * Input:       The program expects all file paths to exist. (Except for the Log and Registry files)
 * Otherwise the program will fail.
 * <p>
 * Hash Algorithm must be "MD5" or "SHA-512"
 * <p>
 * Private and Public key file must be PEM encoded PKCS#8 file.
 * <p>
 * Interval time is an integer. It must be 0-59 minutes.
 * <p>
 * <p>
 * <p>
 * Output:      The output of the program is the log file where the file changes are saved.
 * Log file records which file is deleted, changed, created.
 * At the same time, if the third party changes the registry file, it will write "verification failed".
 * <p>
 * <p>
 * Notes:       none
 */

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 4.11.2017.
 */

public class integrity {

    public static void main(String[] args) {

        // parse all arguments
        CommandArgumentParser p = new CommandArgumentParser(args);
        IntegrityHelper integrityHelper = null;

        if (p.getAction().equalsIgnoreCase(CommandArgumentParser.STOP)) {
            integrityHelper = new IntegrityHelper();
        } else if (p.getAction().equalsIgnoreCase(CommandArgumentParser.START)
                || p.getAction().equalsIgnoreCase(CommandArgumentParser.UNIX_CALL)) {
            integrityHelper = new IntegrityHelper(
                    p.getMonitoringFolderPath(),
                    p.getRegistryFilePath(),
                    p.getLogFilePath(),
                    p.getHashAlgorithm(),
                    p.getPrivateKeyFilePath(),
                    p.getPublicKeyFilePath(),
                    p.getIntervalTime());
        } else {
            Utils.printErrorAndUsage("Invalid program action.", 1);
        }

        // determine who ran the file
        if (p.getAction().equalsIgnoreCase(CommandArgumentParser.START)) {
            integrityHelper.start();
        } else if (p.getAction().equalsIgnoreCase(CommandArgumentParser.STOP)) {
            integrityHelper.stop();
        } else if (p.getAction().equalsIgnoreCase(CommandArgumentParser.UNIX_CALL)) {
            integrityHelper.unixCall();
        } else {
            Utils.printErrorAndUsage("Invalid program action.", 1);
        }
    }
}
