import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 5.11.2017.
 */
public class HashHelper {

    public static final String MD5 = "MD5";
    public static final String SHA512 = "SHA-512";

    private String hashFunction;

    HashHelper(String hashFunction) {
        if (hashFunction.equalsIgnoreCase(MD5)) {
            this.hashFunction = MD5;
        } else if (hashFunction.equalsIgnoreCase(SHA512)) {
            this.hashFunction = SHA512;
        } else {
            Utils.printErrorAndUsage("Invalid Hash Algorithm.", 1);
        }
    }

    /**
     * Read all bytes from given file path and hashes file
     *
     * @param file file path
     * @return hash string
     */
    public String hash(File file) {
        try {
            InputStream fis = new FileInputStream(file);
            byte[] buffer = new byte[8192];
            MessageDigest messageDigest = MessageDigest.getInstance(hashFunction);
            int numRead;

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    messageDigest.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            fis.close();
            byte[] output = messageDigest.digest();
            return Utils.byteToString(output);
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in hashing file: " + e.getMessage(), 1);
        }
        return null;
    }

}
