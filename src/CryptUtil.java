import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 5.11.2017.
 */

public class CryptUtil {

    private static final String SHA512WITHRSA = "SHA512withRSA";
    private static final String MD5WITHRSA = "MD5withRSA";

    private static final String DUMMY = "##signature: DUMMY_SIGNATURE";

    /**
     * Gets all content of registry file except last line(signature line)
     * Gets private key from given private key file path
     *
     * @param registryFile   registry file path
     * @param privateKeyFile private key file path
     * @param hashAlgorithm  MD5 or SHA-512
     * @return signature of content of file
     */
    String signFile(String registryFile, String privateKeyFile, String hashAlgorithm) {

        PrivateKey privateKey = getPrivateKey(privateKeyFile);
        Signature signature = null;

        try {

            if (hashAlgorithm.equalsIgnoreCase(HashHelper.SHA512)) {
                signature = Signature.getInstance(SHA512WITHRSA);
            } else if (hashAlgorithm.equalsIgnoreCase(HashHelper.MD5)) {
                signature = Signature.getInstance(MD5WITHRSA);
            } else {
                Utils.printErrorAndUsage("Invalid hash function: " + hashAlgorithm, 1);
            }

            signature.initSign(privateKey);
            signature.update(getRegistryFileContent(registryFile).getBytes("UTF-8"));
            byte[] signedData = signature.sign();
            return Base64.getEncoder().encodeToString(signedData);

        } catch (NoSuchAlgorithmException e) {
            Utils.printErrorAndUsage("Algorithm not found: " + e.getMessage(), 1);
        } catch (SignatureException e) {
            Utils.printErrorAndUsage("Invalid signature: " + e.getMessage(), 1);
        } catch (InvalidKeyException e) {
            Utils.printErrorAndUsage("Invalid key: " + e.getMessage(), 1);
        } catch (UnsupportedEncodingException e) {
            Utils.printErrorAndUsage("Invalid Encoding: " + e.getMessage(), 1);
        } catch (Exception e) {
            Utils.printErrorAndUsage("Unexpected Error in signing the registry file: " + e.getMessage(), 1);
        }
        return null;
    }

    /**
     * Gets signature from registry file and verifies content of registry file with public key
     *
     * @param registryFile  registry file path
     * @param publicKeyFile private key file path
     * @param hashAlgorithm MD5 or SHA-512
     * @return verified or not
     */
    boolean verifyFile(String registryFile, String publicKeyFile, String hashAlgorithm) {
        PublicKey publicKey = getPublicKey(publicKeyFile);

        Signature signature = null;
        try {
            if (hashAlgorithm.equalsIgnoreCase(HashHelper.SHA512)) {
                signature = Signature.getInstance(SHA512WITHRSA);
            } else if (hashAlgorithm.equalsIgnoreCase(HashHelper.MD5)) {
                signature = Signature.getInstance(MD5WITHRSA);
            } else {
                Utils.printErrorAndUsage("Invalid hash function: " + hashAlgorithm, 1);
            }

            signature.initVerify(publicKey);
            signature.update(getRegistryFileContent(registryFile).getBytes("UTF-8"));
            String registrySignature = getSignatureFromRegistryFile(registryFile);
            byte[] decodedSign = Base64.getDecoder().decode(registrySignature);
            boolean verify = signature.verify(decodedSign);
            return verify;

        } catch (NoSuchAlgorithmException e) {
            Utils.printErrorAndUsage("Algorithm not found: " + e.getMessage(), 1);
        } catch (SignatureException e) {
            Utils.printErrorAndUsage("Invalid signature: " + e.getMessage(), 1);
        } catch (InvalidKeyException e) {
            Utils.printErrorAndUsage("Invalid key: " + e.getMessage(), 1);
        } catch (UnsupportedEncodingException e) {
            Utils.printErrorAndUsage("Invalid Encoding: " + e.getMessage(), 1);
        } catch (Exception e) {
            Utils.printErrorAndUsage("Unexpected Error in verifying the registry file: " + e.getMessage(), 1);
        }
        return false;
    }

    /**
     * Reads and parses private key file
     *
     * @param privateKeyFile key file path
     * @return RSA private key
     */
    private PrivateKey getPrivateKey(String privateKeyFile) {
        try {
            String privateKeyContent = new String(Files.readAllBytes(Paths.get(new File(privateKeyFile).toURI())));
            privateKeyContent = privateKeyContent
                    .replaceAll("\r\n|\n", "")
                    .replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .trim();

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
            return kf.generatePrivate(keySpecPKCS8);

        } catch (UnsupportedEncodingException e) {
            Utils.printErrorAndUsage("Unsupported encoding: " + e.getMessage(), 1);
        } catch (IOException e) {
            Utils.printErrorAndUsage("Error in reading private key file. ", 1);
        } catch (NoSuchAlgorithmException e) {
            Utils.printErrorAndUsage("Algorithm not found: " + e.getMessage(), 1);
        } catch (InvalidKeySpecException e) {
            Utils.printErrorAndUsage("Key spec is invalid: " + e.getMessage(), 1);
        } catch (Exception e) {
            Utils.printErrorAndUsage("Unexpected Error in reading private key from given file path: " + e.getMessage(), 1);
        }
        return null;
    }

    /**
     * Reads and parses public key file
     *
     * @param publicKeyFile key file path
     * @return RSA public key
     */
    private PublicKey getPublicKey(String publicKeyFile) {
        try {
            String publicKeyContent = new String(Files.readAllBytes(Paths.get(new File(publicKeyFile).toURI())));
            publicKeyContent = publicKeyContent
                    .replaceAll("\r\n|\n", "")
                    .replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace("-----END PUBLIC KEY-----", "")
                    .trim();

            KeyFactory kf = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
            return kf.generatePublic(keySpecX509);

        } catch (UnsupportedEncodingException e) {
            Utils.printErrorAndUsage("Unsupported encoding: " + e.getMessage(), 1);
        } catch (IOException e) {
            Utils.printErrorAndUsage("Error in reading private key file. ", 1);
        } catch (NoSuchAlgorithmException e) {
            Utils.printErrorAndUsage("Algorithm not found: " + e.getMessage(), 1);
        } catch (InvalidKeySpecException e) {
            Utils.printErrorAndUsage("Key spec is invalid: " + e.getMessage(), 1);
        } catch (Exception e) {
            Utils.printErrorAndUsage("Unexpected Error in reading public key from given file path: " + e.getMessage(), 1);
        }
        return null;
    }

    /**
     * Gets all content of registry file except last signature line
     *
     * @param registryFilePath registry file path
     * @return content of registry file
     */
    private String getRegistryFileContent(String registryFilePath) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileReader fr = new FileReader(registryFilePath);
            BufferedReader br = new BufferedReader(fr);
            String line;

            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("##signature") && !line.isEmpty()) {
                    stringBuilder.append(line);
                }
            }
            br.close();
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in reading registry file." + e.getMessage(), 1);
        }
        return stringBuilder.toString();
    }

    /**
     * Reads only last signature line from registry file
     *
     * @param registryFile registry file path
     * @return signature string
     */
    private String getSignatureFromRegistryFile(String registryFile) {
        String signatureLine = DUMMY;
        boolean isSignatureLineExist = false;
        try {
            FileReader fr = new FileReader(registryFile);
            BufferedReader br = new BufferedReader(fr);
            String line;

            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("##signature")) {
                    signatureLine = line;
                    isSignatureLineExist = true;
                }
            }
            br.close();
        } catch (Exception e) {
            Utils.printErrorAndUsage("Error in reading registry file." + e.getMessage(), 1);
        }
        if (!isSignatureLineExist) {
            System.out.println("System is not secure. I cant get ##signature from registry file.");
        }
        String[] parse = signatureLine.split(" ");
        return parse[1];
    }

}
