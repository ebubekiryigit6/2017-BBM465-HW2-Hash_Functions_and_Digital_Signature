import java.util.*;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 4.11.2017.
 */

public class CommandArgumentParser {

    static final String START = "start";
    static final String STOP = "stop";
    static final String UNIX_CALL = "unixcall";

    private String monitoringFolderPath;
    private String registryFilePath;
    private String logFilePath;
    private String hashAlgorithm;
    private String privateKeyFilePath;
    private String publicKeyFilePath;
    private String action;
    private int intervalTime; // in minutes

    CommandArgumentParser(String[] commandLine) {
        parseArguments(commandLine);
    }

    private void parseArguments(String[] commandLine) {

        // only start,stop and unix call parameters can run
        if (commandLine.length != 1 && commandLine.length != 14 && commandLine.length != 10) {
            Utils.printErrorAndUsage("Invalid Option.", 1);
        }

        Queue<String> queue = new ArrayDeque<>(Arrays.asList(commandLine));

        // is this an start/stop action or unix call
        String programAction = queue.element().trim();
        if (programAction.equalsIgnoreCase(START)) {
            action = START;
            queue.remove();
            initOptions(queue, false);
        } else if (programAction.equalsIgnoreCase(STOP)) {
            action = STOP;
        } else if (programAction.equalsIgnoreCase("-p")
                || programAction.equalsIgnoreCase("-r")
                || programAction.equalsIgnoreCase("-l")
                || programAction.equalsIgnoreCase("-h")
                || programAction.equalsIgnoreCase("-k")
                || programAction.equalsIgnoreCase("-i")) {
            this.action = UNIX_CALL;
            initOptions(queue, true);
        } else {
            Utils.printErrorAndUsage("Program action can be only <start> or <stop>\nIf this is a unix call, parameters cannot matched.", 1);
        }
    }

    // action is determined. Parse options now.
    private void initOptions(Queue<String> queue, boolean isUnixCall) {
        while (!queue.isEmpty()) {
            String option = queue.remove().trim();
            parseOption(queue, option, isUnixCall);
        }
    }

    // init variables and delete options in queue
    private void parseOption(Queue<String> queue, String option, boolean isUnixCall) {
        try {
            if (option.equalsIgnoreCase("-p")) {
                this.monitoringFolderPath = queue.remove().trim();
            } else if (option.equalsIgnoreCase("-r")) {
                this.registryFilePath = queue.remove().trim();
            } else if (option.equalsIgnoreCase("-l")) {
                this.logFilePath = queue.remove().trim();
            } else if (option.equalsIgnoreCase("-h")) {
                this.hashAlgorithm = queue.remove().trim();
            } else if (option.equalsIgnoreCase("-k")) {
                if (!isUnixCall) {
                    this.privateKeyFilePath = queue.remove().trim();
                }
                this.publicKeyFilePath = queue.remove().trim();
            } else if (option.equalsIgnoreCase("-i") && !isUnixCall) {
                try {
                    this.intervalTime = Integer.parseInt(queue.remove().trim());
                } catch (Exception e) {
                    Utils.printErrorAndUsage("Invalid interval time error.", 1);
                }
            } else {
                Utils.printErrorAndUsage("Invalid Option.", 1);
            }
        } catch (Exception e) {
            System.out.println("Error in command parser: " + e.getMessage());
            Utils.printErrorAndUsage("ou probably entered an argument in the wrong number or format.", 1);
        }
    }

    String getAction() {
        return action;
    }

    String getMonitoringFolderPath() {
        return monitoringFolderPath;
    }

    String getRegistryFilePath() {
        return registryFilePath;
    }

    String getLogFilePath() {
        return logFilePath;
    }

    String getHashAlgorithm() {
        return hashAlgorithm;
    }

    String getPrivateKeyFilePath() {
        return privateKeyFilePath;
    }

    String getPublicKeyFilePath() {
        return publicKeyFilePath;
    }

    int getIntervalTime() {
        return intervalTime;
    }

}
