import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 5.11.2017.
 */
public class Utils {

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public static void printUsage(int exitCode) {
        System.out.println("Usage: "
                + "integrity start/stop\n" +
                "Options:\n" +
                "\t-p <Folder Path>\n" +
                "\t-r <Registry File>\n" +
                "\t-l <Log File>\n" +
                "\t-h <MD5 / SHA512>\n" +
                "\t-k <Private Key File> <Public Key File>\n" +
                "\t-i <Interval Time>\n");
        exit(exitCode);
    }

    public static void exit(int exitCode) {
        System.exit(exitCode);
    }

    public static void printErrorAndUsage(String error, int exitCode) {
        System.out.println(error);
        printUsage(exitCode);
    }

    public static String getDate() {
        return dateFormat.format(new Date());
    }

    public static String byteToString(byte[] output) {
        StringBuilder hexString = new StringBuilder();
        for (byte aMessageDigest : output) {
            String h = Integer.toHexString(0xFF & aMessageDigest);
            while (h.length() < 2)
                h = "0" + h;
            hexString.append(h);
        }

        return hexString.toString();
    }

}
